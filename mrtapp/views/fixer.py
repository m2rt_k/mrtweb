from flask import render_template, request, jsonify, Blueprint
import requests

fixerbp = Blueprint('fixer', __name__)


@fixerbp.route('/fixer')
def fixer():
    return render_template("fixer.html")


@fixerbp.route('/api/fixer/<date>')
def fix(date):
    symbols = [i.strip() for i in request.args.get('symbols', '').split(',')]

    results = []
    for symbol in symbols:
        base = symbol
        current_symbols = [i for i in symbols if i != base]
        res = _get(date, base, current_symbols)
        if res:
            results.append(res)

    data = {
        'date': date,
        'rows': []
    }

    for item in results:
        row = {
            'base': item['base'],
            item['base']: '-'
        }

        for currency, rate in item['rates'].items():
            row[currency] = rate

        data['rows'].append(row)

    return jsonify(data)


@fixerbp.route('/api/fixer/init')
def init():
    res = _get()
    columns = [{
        'field': 'base',
        'title': 'Base',
        'visible': True
    }]

    currencies = [res['base']] + [c for c, r in res['rates'].items()]
    currencies.sort()

    for currency in currencies:
        columns.append({
            'field': currency,
            'title': currency,
            'visible': False
        })

    return jsonify({'columns': columns, 'currencies': currencies})


def _get(date='latest', base=None, symbols=None):
    params = {}

    if base:
        params['base'] = base

    if symbols:
        params['symbols'] = ','.join(symbols)

    req = requests.get(
        url='https://api.fixer.io/' + date,
        params=params
    )

    if req.ok:
        return req.json()
    else:
        return False
