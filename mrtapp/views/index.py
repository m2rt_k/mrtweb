from flask import render_template, Blueprint

indexbp = Blueprint('index', __name__)


@indexbp.route('/')
def index():
    return render_template("index.html")
