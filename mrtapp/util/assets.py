from flask_assets import Bundle, Environment
from .. import app

bundles = {
    'main_js': Bundle(
        'js/lib/jquery-3.3.1.js',
        'js/lib/bootstrap.bundle.js',
        output='gen/main.js',
        filters='jsmin'
    ),

    'main_css': Bundle(
        'css/lib/bootstrap.css',
        output='gen/main.css',
        filters='cssmin'
    ),

    'fixer_js': Bundle(
        'js/lib/bootstrap-table.js',
        'js/lib/bootstrap-datepicker.js',
        'js/lib/bootstrap-select.js',
        'js/m.js'
    ),

    'fixer_css': Bundle(
        'css/lib/bootstrap-datepicker3.css',
        'css/lib/bootstrap-table.css',
        'css/lib/bootstrap-select.css',
        'css/fixer.css'
    )
}

assets = Environment(app)

assets.register(bundles)
