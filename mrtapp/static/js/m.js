(function (code) {
    code(window.jQuery, window, document);
})(function ($, window, document) {
$(function() {
    var butt    = $("#butt"),
        select  = $("#select"),
        table   = $("#table"),
        datep   = $("#date");

    function fillOptions(options) {
        $.each(options, function(i, value) {
            select.append($("<option></option>", {html:value}));
        });
    }

    function hideAllColumns() {
        $.each(table.bootstrapTable('getVisibleColumns'), function(i, column) {
            table.bootstrapTable('hideColumn', column.field);
        });
        table.bootstrapTable('showColumn', 'base');
    }

    datep.datepicker({
        format: 'yyyy-mm-dd',
        endDate: '0d',
        startDate: '1999-01-04'
    });

    butt.on("click", function() {
        var date = datep.datepicker('getFormattedDate');
        date = date ? date : 'latest';
        table.bootstrapTable('removeAll');
        table.bootstrapTable('refresh', {
            url: '/api/fixer/' + date,
            query: {
                symbols: select.val().join()
            }
        })
    });

    select.on("change", function() {
        hideAllColumns();
        $.each(select.val(), function(i, currency) {
            table.bootstrapTable('showColumn', currency);
        });
    });

    $(window).on('resize', function(){
        select.height(Math.max(50, $(this).height() - butt.height() - datep.height() - 175));
        table.bootstrapTable( 'resetView' , {height: $(this).height() - 20} );
    });
    $(window).trigger('resize');


    $.get('/api/fixer/init').done(function (data) {
        fillOptions(data['currencies']);
        table.bootstrapTable({
            columns: data['columns'],
            sortName: 'base',
            sortOrder: 'asc',
            responseHandler: function(res) {
                return res['rows'];
            }
        });
    });
})});