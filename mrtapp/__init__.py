from flask import Flask

app = Flask(__name__)

from .views.index import indexbp
from .views.fixer import fixerbp

app.register_blueprint(indexbp)
app.register_blueprint(fixerbp)

app.config.from_object('config')
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True
from .util import assets
